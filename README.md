# Meibuyu Rpc Library

美不语微服务RPC接口库

## 1、安装

详细版本请看[https://packagist.org/packages/meibuyu/rpc](https://packagist.org/packages/meibuyu/rpc)

```bash  
// 开发时可使用开发版本 dev-xxxx
// 正式上线请打标签,使用正式vA.B.C版本
composer require meibuyu/rpc
```  

---

## 2、使用Rpc

### 1. 创建代理消费者类,在微服务项目config/autoload/services配置文件内进行配置

> 每个服务只需要配置一次,按需配置  
> 详细配置可查看[hyperf自动创建代理消费者类文档](https://hyperf.wiki/2.1/#/zh-cn/json-rpc?id=%e8%87%aa%e5%8a%a8%e5%88%9b%e5%bb%ba%e4%bb%a3%e7%90%86%e6%b6%88%e8%b4%b9%e8%80%85%e7%b1%bb)

```php  
<?php

declare(strict_types=1);

$registry = [
    'protocol' => 'consul',
    'address' => env('CONSUL_URI', 'http://consul:8500'),
];

return [
    'consumers' => [
        [
            // name 需与服务提供者的 name 属性相同,硬性规定name是接口名去掉Interface
            'name' => 'UserService',
            // 服务接口名, 即服务接口类
            'service' => Meibuyu\Rpc\Service\Interfaces\User\UserServiceInterface::class,
            // 从consul服务中心获取节点信息
            'registry' => $registry,
        ]
    ]
];
```  

### 2. 在代码中调用

```php  
/**
 * @Inject()
 * 依赖注入用户服务类
 * @var UserServiceInterface
 */
private $userService;

public function test()
{
    // 调用用户服务get方法
    $user = $this->userService->get(1);
}
```  

---  

## 3、创建Rpc

### 1. 新建Rpc服务接口

- 拉取本项目至本地,在对应src/Service/Interfaces文件夹中创建接口文件
- 文件以项目名分文件夹,文件名**必须**以Interface结束,且注意分配相关项目的命名空间
- 方法名**必须**写注释,参数和返回数据类型必须写,方便使用者使用

```php  
<?php

namespace Meibuyu\Rpc\Service\Interfaces\Test;

interface TestServiceInterface
{
    /**
     * 测试接口
     * @param int $id ID
     * @param string $name 名称
     * @return array
     */
    public function test(int $type, string $number): array;
}
```  

### 2. 为已有服务接口加入新方法/修改已有方法

- 在现有接口类文件上编写新方法,或者修改已有方法
- 方法名必须写注释,参数和返回数据类型必须写,方便使用者使用

### 3. 在服务提供方的项目代码中实现接口类方法

> 类名**必须**是接口类去除Interface后的名称  
> 详细配置可查看[hyperf定义服务提供者文档](https://hyperf.wiki/2.1/#/zh-cn/json-rpc?id=%e5%ae%9a%e4%b9%89%e6%9c%8d%e5%8a%a1%e6%8f%90%e4%be%9b%e8%80%85)

```php  
<?php

namespace App\Rpc;

/**
 * @RpcService(name="TestService", protocol="jsonrpc-http", server="jsonrpc-http", publishTo="consul")
 */
class TestService implements TestServiceInterface
{
    /**
     * 测试接口
     * @param int $id ID
     * @param string $name 名称
     * @return array
     */
    public function test(int $type, string $number): array
    {
        // 实现方法
        return $name . $id; 
    }
}
```  

---

### 4. 发布分支/发布版本  
- 在接口类和方法实现后,提交代码至develop分支
- 服务提供者和服务使用者,同时更新项目的composer.json文件中meibuyu/rpc版本至开发版本dev-develop,进行测试
- 测试完成,提交合并请求至master分支,并让此项目维护者合并,并且打版本发布
- 服务提供者和服务使用者,同时更新项目的composer.json文件中meibuyu/rpc版本至正式版本  
```base
// "meibuyu/rpc": "dev-master", // 开发版本
// "meibuyu/rpc": "~1.0.0",     // 正式版本

// 更新指定包
composer update meibuyu/rpc
```
