<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\User;

interface BaseInfoServiceInterface
{

    /**
     * 通过单个id获取岗位数组
     * @param int $id
     * @return array
     */
    public function getPositionListById(int $id): array;

    /**
     * 通过单个id获取岗位向上取数组
     * @param int $id
     * @param null $level
     * @return array
     */
    public function getPositionWithTopListById(int $id, $level = null): array;

    /**
     * 通过id数组获取国家数组
     * @param array $idList 默认去重
     * @param array $columns
     * @return array 默认keyBY('id')
     */
    public function getCountryListByIdList(array $idList, array $columns = ['*']): array;

    /**
     * 通过id数组获取国家区域数组
     * @param array $ids 默认去重
     * @param array $relations ['country'] 可关联国家数据
     * @return array 默认keyBY('id')
     */
    public function getCountryAreaListByIdList(array $ids, array $relations = []): array;

    /**
     * 通过id数组获取团队数组
     * @param array $idList 默认去重
     * @param array $columns
     * @return array 默认keyBY('id')
     */
    public function getTeamListByIdList(array $idList, array $columns = ['id', 'name']): array;

    /**
     * 通过id数组获取站点数组
     * @param array $idList 默认去重
     * @param array $columns
     * @param array $relations $relations 支持的关联关系 ['team', 'country'] 分别代表 团队、国家、
     * @return array 默认keyBY('id')
     */
    public function getSiteListByIdList(array $idList, array $columns = ['id', 'name'], array $relations = []): array;

    /**
     * 通过id数组获取货币数组
     * @param array $idList 默认去重
     * @param array $columns
     * @return array 默认keyBY('id')
     */
    public function getCurrencyListByIdList(array $idList, array $columns = ['id', 'name']): array;

    /**
     * 通过Keyword获取货币id数组
     * @param $keyword
     * @return array
     */
    public function getCurrencyIdsByKeyword($keyword);

    /**
     * 通过单个id获取岗位信息
     * @param int $id 职位id
     * @param array $relations 职位的关联信息 支持["position_level","users","parent"
     * ,"children","perms"] 分别是 岗位职级，岗位用户，岗位父级，岗位子集，岗位对应的权限
     * @param array $columns 默认显示所有字段
     * @return array|null
     */
    public function getPositionById(int $id, array $relations = [], array $columns = ['*']);

    /**
     * 根据id获取单个数据
     * @param int $id 货币id
     * @param array $columns 要显示的字段 默认全部 ['id', 'name', 'code', 'symbol']
     * @return array|null
     */
    public function getCurrencyById($id, array $columns = ['id', 'name', 'code', 'symbol']);

    /**
     * 根据id获取单个数据
     * @param int $id
     * @param array $columns 要显示的字段 默认全部
     * @return array|null
     */
    public function getCountryById($id, array $columns = ['*']);

    /**
     * 获取单个团队数据
     * @param int $id
     * @param array $relations 支持的关联关系 ['leader', 'sites', "users", "parent", "children"] 分别代表 负责人、团队下的站点、团队成员、父级团队，再级团队
     * @param array $columns 要显示的字段 默认['id', 'pid', "name", "leader_user_id",  "department_id"]
     * @return array|null
     */
    public function getTeamById($id, array $relations = [], array $columns = ['id', 'pid', "name", "leader_user_id", "department_id"]);

    /**
     * 获取单个站点数据
     * @param int $id
     * @param array $relations $relations 支持的关联关系 ['team', 'country'] 分别代表 团队、国家、
     * @param array $columns 要显示的字段 默认['id', "name", "url", "country_id", "team_id"]
     * @return array|null
     */
    public function getSiteById($id, array $relations = [], array $columns = ['id', "name", "url", "country_id", "team_id"]);

    /**
     * 根据团队id获取对应的站点列表
     * @param int $teamId 团队id
     * @param array $relations $relations 支持的关联关系 ['team', 'country'] 分别代表 团队、国家、
     * @param array $columns 要显示的字段 默认['id', "name", "url", "country_id", "team_id"]
     * @return array|null
     */
    public function getSiteListByTeamId($teamId, array $relations = [], array $columns = ['id', "name", "url", "country_id", "team_id"]);

    /**
     * 返回所有货币数据
     * @param array $columns 要显示的字段
     * $columns = ['id', 'name', 'code', 'symbol'];
     * @return array 默认已keyBy('id')
     */
    public function currencies(array $columns = ['id', 'name']): array;

    /**
     * 获取所有国家数据
     * @param array $columns 要显示的字段
     * $columns = ['id', 'name', 'iso_code2', 'iso_code3'];
     * @return array 默认已keyBy('id')
     */
    public function countries(array $columns = ['id', 'name']): array;

    /**
     * 获取所有团队数据
     * @param array $relations 支持的关联关系 ['leader', 'sites', "users", "parent", "children"] 分别代表 负责人、团队下的站点、团队成员、父级团队，再级团队
     * @param array $columns 要显示的字段
     * $columns = ['id', 'pid', "name", "leader_user_id", "leader_user_id", "department_id"];
     * @return array 默认已keyBy('id')
     */
    public function teams(array $relations = [], array $columns = ['id', 'pid', "name", "leader_user_id", "department_id"]): array;

    /**
     * 获取所有站点的数据
     * @param array $relations 支持的关联关系 ['team', 'country'] 分别代表 团队、国家
     * @param array $columns 要显示的字段
     * $columns = ['id', "name", "url", "country_id", "team_id"];
     * @return array 默认已keyBy('id')
     */
    public function sites(array $relations = [], array $columns = ['id', "name"]): array;

    /**
     * 获取部门领导id数据
     * @param array $ids 部门id数组
     * @return array
     */
    public function getDepartmentLeaderIdsByIds($ids);

    /**
     * 获取审阅人员
     * @param $teamId
     * @param $authId
     * @return array|bool
     */
    public function getReviewUsers($teamId, $authId);

    /**
     * 通过id数组获取部门数组
     * @param array $idList 默认去重
     * @param array $relations 关联关系,默认空 ['users', 'leader']
     * @param array $columns 要显示的字段,默认全部 ['id', 'name', 'pid', 'remark']
     * @return array 默认keyBY('id')
     */
    public function getDepartmentListByIdList(array $idList, array $relations = [], array $columns = ['*']): array;

    /**
     * 获取单个部门数据
     * @param int $id
     * @param array $relations 关联关系,默认空 ['users', 'leader']
     * @param array $columns 要显示的字段,默认全部 ['id', 'name', 'pid', 'remark']
     * @return array|null
     */
    public function getDepartmentById($id, array $relations = [], array $columns = ['*']);

    /**
     * 获取所有部门数据
     * @param array $relations 关联关系,默认空 ['users', 'leader']
     * @param array $columns 要显示的字段,默认全部 ['id', 'name', 'pid', 'remark']
     * @return array 默认已keyBy('id')
     */
    public function departments(array $relations = [], array $columns = ['*']): array;

    /**
     * 通过职级id数组获取用户占比数组
     * @param array $positionLevelIds 职级id数组,默认去重
     * @return array
     * @author Zero
     */
    public function getRateByPositionLevelIds(array $positionLevelIds): array;

    /**
     * 通过单个id获取国家地区信息
     * @param int $id 职位id
     * @param array $columns 默认显示所有字段
     * @return array|null
     */
    public function getCountryZoneById(int $id, array $columns = ['*']);

    /**
     * 根据国家code数组获取国家数组
     * @param array $codes 国家code数组
     * @param array $columns 获取的字段,默认全部
     * @param int $codeType 2/3 (iso_code2/iso_code3)
     * @return array 默认已keyBy('iso_code')
     * @author Zero
     */
    public function getCountriesByCodes(array $codes, array $columns = ['*'], $codeType = 2);

    /**
     * 根据国家地区code数组获取国家地区数组
     * @param array $codes 国家code数组
     * @param null $countryId 国家id
     * @param array $relations 关联关系,默认空 可填['country']
     * @return array
     * @author Zero
     */
    public function getCountryZonesByCodes(array $codes, $countryId = null, array $relations = []);

}
