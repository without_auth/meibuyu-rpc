<?php
/**
 * Created by PhpStorm.
 * User: qjk
 * Date: 2022/03/28
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\User;

interface AuthenticationInterface
{

    /**
     * 获取对应用户能够看到的菜单
     * @param string $applicationName
     * @param integer $userId
     * @return array
     */
    public function getMenus($applicationName, $userId): array;


    /**
     * 获取对应用户的菜单权限
     * @param string $applicationName 应用名称
     * @param integer $userId 用户 ID
     * @return array
     */
    public function getButtons($applicationName, $userId, $frontendPath):array;


    /**
     * 获取对应路由的接口权限结果
     * @param $path string 请求 URL 路径
     * @param $method string 路由名字
     * @param $applicationName string 应用名字
     * @param $userId integer  用户 ID
     * @return bool
     */
    public function authByRouter($applicationName, $path, $method, $userId):  bool;
}
