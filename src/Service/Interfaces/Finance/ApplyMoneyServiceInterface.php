<?php
/**
 * Created by PhpStorm.
 * User: qiudongfang
 * Date: ${DATA}
 * Time: 下午2:57
 */

namespace Meibuyu\Rpc\Service\Interfaces\Finance;

interface ApplyMoneyServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function createApplyMoney($data);

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function updateApplyMoney($id, $data);
}
