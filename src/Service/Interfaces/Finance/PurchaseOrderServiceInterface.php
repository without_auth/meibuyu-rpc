<?php
/**
 * Created by PhpStorm.
 * User: qiudongfang
 * Date: ${DATA}
 * Time: 下午2:57
 */

namespace Meibuyu\Rpc\Service\Interfaces\Finance;

interface PurchaseOrderServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function syncOAPurchaseOrder($data);

    /**
     * @param array $data
     * @return mixed
     */
    public function createPurchaseOrder($data);

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function updatePurchaseOrder($id, $data);

    /**
     * @param array $ids
     * @return mixed
     */
    public function deletePurchaseOrder($ids);
}
