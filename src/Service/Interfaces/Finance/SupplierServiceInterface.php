<?php
/**
 * Created by PhpStorm.
 * User: qiudongfang
 * Date: ${DATA}
 * Time: 下午2:51
 */

namespace Meibuyu\Rpc\Service\Interfaces\Finance;

interface SupplierServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function createSupplier($data);

    /**
     * @param integer $id
     * @param array $data
     * @return mixed
     */
    public function updateSupplier($id, $data);
}
