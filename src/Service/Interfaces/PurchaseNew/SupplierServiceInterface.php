<?php


namespace Meibuyu\Rpc\Service\Interfaces\PurchaseNew;


interface SupplierServiceInterface
{




    /**
     * 供应商列表数据
     * @param  array 供应商id数组
     * @param  $params['company_name'] 公司名称
     * @return
     */
    public function list(array $params): string;

//    /**
//     * 供应商下拉框
//     * @return array
//     */
//
//    public function getCompanyOption(): string;

}