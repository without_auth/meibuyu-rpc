<?php


namespace Meibuyu\Rpc\Service\Interfaces\PurchaseNew;


interface SupplierNewServiceInterface
{




    /**
     * 供应商列表数据
     * @param  array 供应商id数组
     * @param  $params['company_name'] 公司名称
     * @return
     */
    public function list(array $params);

//    /**
//     * 供应商下拉框
//     * @return array
//     */
//
//    public function getCompanyOption(): string;

    /**
     * 根据supplier_id 获取供应商信息
     * @param array $params [1,2,3]
     * @return array
     */
    public function getSupplierInfo(array $params): array;

    /**
     * 通过供应商名称获取供应商信息
     * @param array $supplierName
     * @return array
     */
    public function getSupplierInfoByName(array $supplierName): array;
	
	  /**
     * 需要兼容的供应商
     * @return array
     */
	public function supplierMap();
}