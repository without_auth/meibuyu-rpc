<?php


namespace Meibuyu\Rpc\Service\Interfaces\PurchaseNew;


interface AbnormalServiceInterface
{



    /**
     * 到仓异常数据添加
     * @param $scanNo
     * @return mixed
     */
    public function createWarehoused($params): string;

}