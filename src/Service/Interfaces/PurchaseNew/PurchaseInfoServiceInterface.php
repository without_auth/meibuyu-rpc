<?php


namespace Meibuyu\Rpc\Service\Interfaces\PurchaseNew;


interface PurchaseInfoServiceInterface
{


    /**
     * 同步请款状态
     * 审核通过
     * {
         "purchase_id":"采购单id",
     *   "reject_reason":"驳回缘由", 审核驳回需要填充
     *   "flag":true|false  //true审核通过 false审核驳回
     * }
     * @return mixed
     */
    public function syncApplyMoneyStatus($post);

    /**
     * 扫描物流号或批次号查询
     * @param $scanNo
     * @return array
     */
    public function getShippingInfo($scanNo): array;

    /**
     * 分页查询
     * @param int $page 当前第几页
     * @param int $pageSize 每页显示数
     * @return array
     */
    public function getShippingList($page=1,$pageSize=10): array;


    /**
     * 根据批次号查询采购信息
     * @param array $batchNo 批次号数组
     * @return array
     */
    public function getPurchaseInfoArrByBatchNo(array $batchNo) :array;




    /**
     * 子订单号获取物流信息
     * @param  array $subOrderNos
     * @return array
     */
    public function getLogisticsInfo(array $subOrderNos);



    /**
     * 财务核算完成同步采购更新状态
     * @param $purchaseId
     * @return mixed
     */
    public function syncAccountingComplete($purchaseId):bool;





}