<?php


namespace Meibuyu\Rpc\Service\Interfaces\PurchaseNew;


interface PurchaseReturnsServiceInterface
{
    /**
     * 获取对应的退货列表
     * @param array $filters
     * @return array
     */
    public function getReturnsByFilter(array $filters = []): array;

    /**
     * 退货操作
     * @param $params
     * @return array
     */
    public function returnProducts($params): array;
}