<?php


namespace Meibuyu\Rpc\Service\Interfaces\PurchaseNew;


interface PurchaseStockServiceInterface
{

    /**
     * 收货记录rpc
    {

    "batch_no": "batch_no",
    "purchase_no": "1634197133",
    "product": [{
    "id": 24,
    "received_num":22 //收货数量
    }]
    }
     *
     *
     * response
     * {
    "flag"=>true|false
     *  "error_msg"=>''
     *  }
     * @param $post
     * @return array
     */
    public function receiptOperation($post) :array ;


    /**
     * 入库记录rpc
    {
    "batch_no": "batch_no",
    "purchase_no": "1634197133",
    "stored_person":"入库操作人"
    "product": [{
    "id": 24,
    "stored_num":22 //入库数量
    }]
    }
     * @param $post
     * @return array
     */
    public function warehousingOperation($post):array;


    /**
     * 已完成记录rpc 已弃用
     *
    {
    "batch_no": "batch_no",
    "purchase_no": "1634197133",
    "product": [{
    "id": 24,
    "completed_num":22 //已完成数量
    }]
    }
     * @param $post
     * @return array
     */
    public function completeOperation($post):array;


    /**
     * 退款记录rpc
    {
    "id": 28,
    "purchase_id": 26,
    "purchase_no": "1634197133",
    "product": [{
    "id": 24,
    "return_num":22 //已退货数量
    }]
    }
     * @param $post
     * @return array
     */
    public function refundOperation($post):array;


    /**
     * 获取采购在途的数量总和
     * @return mixed
     */
    public function getShippingCount();

}