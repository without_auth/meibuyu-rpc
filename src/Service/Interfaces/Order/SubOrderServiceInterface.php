<?php
/**
 * Created by PhpStorm.
 * User: zhaopeng
 * Date: 2020/9/1
 * Time: 10:09
 */

namespace Meibuyu\Rpc\Service\Interfaces\Order;

interface SubOrderServiceInterface
{

    /**
     * @param int $id
     * @param array $column 需要查询的字段
     * @param array $relation 需要的子订单关联关系可传入['order','order_product']
     * @return array | null
     */
    public function getById(int $id, array $column = ['*'], array $relation = []): array;

    /**
     *
     * @param array $idList 子订单的ID  数组[1,2,3]
     * @param array $column 需要查询字段
     * @param array $relation 需要的关联关系可传入['order','order_product']
     * @return array | null
     */
    public function getByIdList(array $idList, array $column = ['*'], array $relation = []): array;

    /**
     * @param array $idList 需要改变发货状态的子订单ID数组
     * @param int $status 需要改变的发货状态ID 1 待发货 2 已发货 3 已签收 4 已取消
     * @return bool
     */
    public function updateSubOrderShipStatus(array $idList, int $status): bool;

    /**
     * 需要改变的子订单id
     * @param int $id
     * @return bool
     */
    public function StockIntoUpdateSubOrderStatus(int $id): bool;

    /**
     * description:通过来源单号获取生产工厂
     * author: fuyunnan
     * @param
     * @return array
     * @throws
     * Date: 2020/10/29
     */
    public function getBySourcesFactory($sources): array;

    /**
     *
     * @param array $source 来源单号数组
     * @return array
     */
    public function getBySourceSite($source): array;

    /**
     * 订单采购完成 修改子订单信息  ---1688采购系统使用
     * @param $data //修改参数数组(二维数组)
     *     参数字段:$data = [
     *        [
     *          'sub_order_no'=>oa子订单编号
     *          'supplier_name'=>供应商，
     *          'purchase_price'=>采购总价,
     *          'platform_order'=>'采购平台订单号',
     *          'domestic_logistics_no'=>物流单号,
     *          'domestic_logistics_price'=>物流价格
     *        ],
     *        [
     *           'sub_order_no'=>oa子订单编号
     *           'supplier_name'=>供应商，
     * 'purchase_price'=>采购总价,
     * 'platform_order'=>'采购平台订单号',
     * 'domestic_logistics_no'=>物流单号,
     * 'domestic_logistics_price'=>物流价格
     * ]
     *      ]
     * @return bool
     */
    public function purchaseCompleted($data, $type): bool;

    /**
     * 1688采购异常订单 修改OA子订单状态
     * @param $orderId //oa子订单id
     * @param $errorCode //异常信息 1 取消 2其他
     * @return array
     */
    public function purchaseError($orderId, $errorCode): array;

    /**
     * 1688采购取消
     * @param $orderNo
     * @param $editData
     * @return bool
     */
    public function purchaseCancel($orderNo, $editData = []): bool;

    /**
     * 1688采购 修改oa子订单
     * @param $editData
     * $editData = [
     *           'logistic_no'=>'需要修改的物流单号'
     *           'logistic_no_new'=>'修改后的物流单号',
     *           'logistic_price'=>'物流费用',
     *       ]
     * @return array
     */
    public function purchaseEdit($editData): bool;

    /**
     * 删除物流信息
     * @param $data =>[
     *          'sub_order_no'=>'子订单号',
     *          'logistic_no'=>'物流单号'
     *       ]
     * @return bool
     */
    public function purchaseDelete($data): bool;

    /**
     * 通过子订单获取出库信息
     * @param array $subOrderNos
     * @return array （返回满足出库条件的子订单号以及出库数量 keyBy sub_order_no）
     */
    public function isOutStockInfo(array $subOrderNos = []):array;

    /**
     * 通过子订单号获取子订单信息
     * @param array $subOrderNos
     * @return array
     */
    public function getSubOrderInfoByNo(array $subOrderNos = [], array $column = ['sub_order_no','shipping_notes','dispatch_notes','product_specification']): array;

}
