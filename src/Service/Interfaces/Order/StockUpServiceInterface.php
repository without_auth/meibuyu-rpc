<?php

namespace Meibuyu\Rpc\Service\Interfaces\Order;

interface StockUpServiceInterface
{
    /**
     *自动备货入库后改变备货单状态为已入库
     * @param $stockNo
     * @return mixed
     */
    public function stockIntoUpdateStatus($stockNo): bool;

    /**
     * 跟进物流单号
     * @param $logisticsNo
     * @return array
     */
    public function getStockUpInfoByLogisticsNo($logisticsNo): array;

    /**
     *
     * @param $sourceNo
     * @return array
     */
    public function getStockUpInfoByOrderNo($sourceNo): array;
}
