<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2021/06/11
 * Time: 14:22:52
 */

namespace Meibuyu\Rpc\Service\Interfaces\Purchase;

interface PurchaseTaskServiceInterface
{

    /**
     * 为订单提供获取采购数据
     * @param array $noList
     * @return array
     * @author Zero
     */
    public function infoForOrder(array $noList);

    /**
     * 通知取消
     * @param array $data
     * [
     *     'source_id' => '来源id',
     *     'notice_cancel_reason' => '通知取消原因',
     *     'type' => 1 // 1(订单采购); 2(备货采购); 3(样品采购)
     *     'auth' => Auth::user() // 当前用户
     * ]
     * @return bool
     * @throws \Meibuyu\Rpc\Exceptions\RpcException
     * @author Zero
     */
    public function noticeCancel(array $data);

    /**
     * 更新产品数据
     * @param mixed $sourceId 来源id
     * @param array $data 要更新的数据如['specification', 'remark']等
     * @param int $type 采购类型,默认为订单采购,可传参数[1(订单采购); 2(备货采购); 3(样品采购)]
     * @return bool
     * @throws \Meibuyu\Rpc\Exceptions\RpcException
     * @author Zero
     */
    public function updateProduct($sourceId, array $data, $type = 1);

}