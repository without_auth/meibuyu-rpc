<?php

namespace Meibuyu\Rpc\Service\Interfaces\Purchase;

interface PurchaseDoneServiceInterface
{
    /**
     *通过子订单编号修改采购状态
     * @param array $orderIds 子订单编号 ['3333444','12222']
     * @return mixed
     */
    public function archivePurchase($orderIds);

    /**
     *通过子订单编号通知取消
     * @param array $orderIds 子订单编号 ['3333444','12222']
     * @return mixed
     */
    public function noticeToCancel($orderIds);

    /**
     * 传输产品规格
     * @param $data ['order_id'=>'子订单编号','order_standard'=>'产品规格']
     * @return mixed
     */
    public function productStandard($data);


    /**
     * 通过子订单编号查询状态
     * @param array $orderIds 子订单编号 ['3333444','12222']
     * @return mixed
     */
    public function getOrderStatus($orderIds);
}
