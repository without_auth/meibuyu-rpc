<?php

namespace  Meibuyu\Rpc\Service\Interfaces\FeiShu;

interface FeiShuMessageServiceInterface
{
    /**
     * Notes: 批量发送消息
     * User: carlos
     * DateTime: 2021/12/15 17:31
     * @param array $userIds
     * @param string $template
     * @param array $data
     * @return mixed
     */
    public function batchSendMsg(array $userIds, string $template, array $data);


    /**
     * Notes: 发送markdown消息
     * User: carlos
     * DateTime: 2022/1/3 11:25
     * @param string $title 标题
     * @param string $content 内容(markdown格式)
     * @param string $template 模板名称 可以为空
     * @param string $application 应用名称
     * @param array $receiveUserId 接收人
     * @param int $sendUserId 发送人
     * @return mixed
     */
    public function sendMessage(string $title, string $content, string $template, string $application, array $receiveUserId, int $sendUserId = 0);
}