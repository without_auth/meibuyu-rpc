<?php

namespace  Meibuyu\Rpc\Service\Interfaces\FeiShu;

Interface FeiShuUserServiceInterface
{
    /**
     * Notes: 通过手机号码 获取飞书用户ID
     * User: carlos
     * DateTime: 2021/12/15 13:50
     * @param string $phone
     * @return mixed
     */
    public function getUserIdByPhone(array $phone);

    /**
     * Notes: 获取部门用户
     * User: carlos
     * DateTime: 2021/12/15 13:51
     * @return mixed
     */
    public function getUserList();

    /**
     * Notes:通过临时Code获取用户信息(用于飞书扫码登录第三方应用)
     * User: carlos
     * DateTime: 2021/12/20 9:31
     * @param string $code
     * @return mixed
     */
    public function getUserByCode(string $code);

    /**
     * Notes: 获取飞书应用id
     * User: carlos
     * DateTime: 2021/12/20 9:32
     * @return mixed
     */
    public function getAppId();
}