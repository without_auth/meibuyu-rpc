<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/3/16
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\Store;

interface StorePurchaseServiceInterface
{

    /**
     * 继续入库
     * [
     *  'purchase_no' => '',       //采购单号（必填）
     *  'batch_no'    => '',       //批次号（必填）
     *  'extra'       => [],       //后续扩展使用可以，非必填
     * ]
     * @param array $params
     * @return array
     * [
            'msg' => '退货成功',
            'code'=> 200,                       //响应码
            'data' => []                        //返回的数据
        ];
     */
    public function continueWarehousing(array $params): array;


    /**
     * 单个产品继续入库
     * [
     *  'detail_id' => 1221'
     *  'num'       => 10,       //批次号（必填）
     * ]
     * @param array $params
     * @return array
     * [
        'msg'  => '继续入库成功',
        'code' => 200,
        'data' => []
      ];
     */
    public function productContinueWarehousing(array $params): array;
}
