<?php
/**
 * Created by PhpStorm. 仓库原料rpc
 * User: fuyunnan
 * Date: 2020/3/16
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\Store;

interface StoreMaterialServiceInterface
{
    /**
     * description:创建原料入库单
     *
     * data[master][warehousing_date]:2020-01-08 入库时间
     * data[master][creator_id]:12 创建人id
     * data[master][warehouse_id]:2 仓库id
     * data[master][type_id]:1 入库单类型
     * data[master][source_no]:no_121333 来源单号 （选填）
     * data[master][remark]:备注 （选填）
     * data[master][status]:2 状态 （选填 不填默认1）
     *
     * 产品二维数组
     * data[goods][0][material_id]:16 产品id
     * data[goods][0][should_cnt]:133 应入数量
     * data[goods][0][real_cnt]:10 实入数量
     * data[goods][1][material_id]:18
     * data[goods][1][should_cnt]:10
     * data[goods][1][real_cnt]:15
     *
     * author: fuyunnan
     * @param array $attributes 需要入库的数组 格式请参考yapi 入库添加
     * @return array
     * @throws
     * Date: 2020/7/6
     */
    public function createMaterialWarehousing(array $attributes): array;


    /**
     * description:批量创建入库单
     * author: fuyunnan
     * @param
     * @return array
     * @throws
     * Date: 2020/10/31
     */
    public function createBatchMaterialWarehousing($attributes): array;

    /**
     * description:批量创建出库单
     * author: fuyunnan
     * @param
     * @return array
     * @throws
     * Date: 2020/10/31
     */
    public function createBatchExMaterialWarehouse($attributes): array;

    /**
     * description:批量修改入库单
     * author: fuyunnan
     * @param
     * @return array
     * @throws
     * Date: 2020/10/31
     */
    public function updateBatchMaterialWarehousing($attributes): array;

    /**
     * description:批量修改出库单
     * author: fuyunnan
     * @param
     * @return array
     * @throws
     * Date: 2020/10/31
     */
    public function updateBatchExMaterialWarehouse($attributes): array;

    /**
     * description:通过原料id数组获取库存列表 给订单系统查询的接口 返回当前原料分组后的库存
     * author: fuyunnan
     * @param array $ids 原料ids 数组
     * @param array $wareIds 仓库数组id
     * @return array
     * @throws
     * Date: 2020/7/27
     */
    public function getGroupMaterialStock($ids, $wareIds = []): array;

    /**
     * description:批量查看入库单信息
     * author: fuyunnan
     * @param array $ids 入库单ids数组
     * @param array $relations 关联关系 ['material_warehousing_order_materials']
     * @return array
     * @throws
     * Date: 2020/10/31
     */
    public function showBatchWarehousing($ids, $relations = []): array;

    /**
     * description:批量查看出库单信息
     * author: fuyunnan
     * @param array $ids 出库单ids数组
     * @param array $relations 关联关系 ['material_ex_warehouse_order_materials']
     * @return array
     * @throws
     * Date: 2020/10/31
     */
    public function showBatchExeWarehouse($ids, $relations = []): array;


    /**
     * description:批量调拨库存方法
     * author: fuyunnan
     * @param array $data 表单
     * @return array
     * @throws
     * Date: 2020/11/3
     */
    public function transferToStock($data): array;

    /**
     * description:整合 先出库 然后 再入库数量 失败回滚
     * author: fuyunnan
     * @param array $outData 出库数组
     * @param array $inData 出库数组
     * @return array
     * @throws
     * Date: 2020/11/5
     */
    public function transferUpdateOutAfterIn($outData, $inData): array;
}
