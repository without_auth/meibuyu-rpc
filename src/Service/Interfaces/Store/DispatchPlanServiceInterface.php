<?php
/**
 * Created by PhpStorm.
 * User: 姜克保
 * Date: 2020/5/20
 * Time: 15:48
 */

namespace Meibuyu\Rpc\Service\Interfaces\Store;


interface DispatchPlanServiceInterface
{
    /**
     * 返回发货计划存在的仓库子产品id
     * @param array $idList 仓库子产品id
     * @return array
     */
    public function getExistProductChildIds(array $idList): array;

    /**
     * 返回发货计划存在的平台子产品id
     * @param array $idList
     * @return array
     */
    public function getExistPlatformProductChildIds(array $idList): array;

}
