<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/3/27
 * Time: 15:03
 */

namespace Meibuyu\Rpc\Service\Interfaces\App;

interface AppServiceInterface
{

    /**
     * 获取当前用户可访问的应用数组
     * @param $user
     * @param bool $isSuperAdmin 是否是超级管理员
     * @return mixed
     */
    public function getAccessApps($user, $isSuperAdmin = false);

    /**
     * 获取单个数据
     * @param int $id
     * @param array $relations 关联关系只有['group']
     * @param array $columns
     * @return mixed
     */
    public function get(int $id, array $relations = [], array $columns = ['id', 'title']);

    /**
     * 通过id列表获取应用数组
     * @param array $idList 默认去重
     * @param array $relations 关联关系只有['group']
     * @param array $columns 默认展示id和title,可传['title', 'name', 'entry', 'prefix', 'group_id', 'is_inside', 'is_active', 'icon', 'desc', 'weight']
     * @return array 默认keyBy('id')
     */
    public function getByIdList(array $idList, array $relations = [], array $columns = ['id', 'title']): array;

    /**
     * 通过name列表获取应用数组
     * @param array $nameList 默认去重
     * @param array $relations 关联关系只有['group']
     * @param array $columns 默认展示id和title,可传['title', 'name', 'entry', 'prefix', 'group_id', 'is_inside', 'is_active', 'icon', 'desc', 'weight']
     * @return array 默认keyBy('id')
     */
    public function getByNameList(array $nameList, array $relations = [], array $columns = ['id', 'title', 'name']): array;

}
