<?php
/**
 * Created by PhpStorm.
 * User: Jack
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\Wms;

interface ChannelAndCompanyInterface
{
    /**
     * 通过物流渠道id获取物流渠道
     * @param array $channelIds $channelIds：渠道id数组， 格式：[1,3,5]
     * @return array
     */
    public function getChannelByIds(array $channelIds): array;

      /**
     * 通过公司id获取公司信息
     * @param array $companyIds $param：公司ids数组、公司名称， 格式：
     *   $post=[
     *       'ids'=>[1,2,3],
     *       'name'=>'华东急速国际',
     *       ];
     *
     * @return array
     */
    public function getCompanyByIds(array $companyIds): array;
}
