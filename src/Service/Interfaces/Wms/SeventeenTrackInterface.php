<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\Wms;

interface SeventeenTrackInterface
{
    /**
     * 从17track实时获取物流信息是否签收
     * @param array $trackNumber $trackNumber：物流单号，$carrier 17track物流公司编号 格式：[['number' => $trackNumber,'carrier' => $carrier]]
     * @return array
     */
    public function getRealTimeSignForNo(array $trackNumber): array;

    /**
     * 从wms缓存表获取物流信息是否签收
     * @param array $trackNumber $trackNumber：物流单号 格式：[$trackNumber1,$trackNumber2]
     * @return array
     */
    public function getCacheSignForNo(array $trackNumber): array;
}
