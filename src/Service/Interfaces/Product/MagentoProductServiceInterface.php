<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/10/12
 * Time: 9:39
 */

namespace Meibuyu\Rpc\Service\Interfaces\Product;

interface MagentoProductServiceInterface
{

    /**
     * 通过magento子产品的属性获取仓库单个产品信息
     * @param string $platformProductSku magento 平台主sku
     * @param string $color 颜色
     * @param string $size 尺码
     * @param int    $siteId
     * @return array
     */
    public function getChildByAttributes(int $siteId,string $platformProductSku, string $color, string $size): array;

    /**
     * 通过magento子产品的属性获取仓库批量产品信息
     * @param int $siteId
     * @param array $attributes
     * [['platform_product_sku' => $platformProductSku, 'color' => $color, 'size' => $size]]
     * @return array
     */
    public function getChildrenByAttributes(int $siteId,array $attributes): array;

}
