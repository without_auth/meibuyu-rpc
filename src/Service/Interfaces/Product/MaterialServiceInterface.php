<?php
/**
 * Created by PhpStorm.
 * User: 梁俊杰
 * Date: 2020/5/15
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\Product;

interface MaterialServiceInterface
{

    /**
     * 获取单个数据
     * @param int $id 原料id
     * @param array $columns 原料表的字段，默认显示全部
     * @return array|null
     */
    public function get($id, array $columns = ['*']);

    /**
     * 通过id列表获取原料数组
     * @param array $idList 原料id的列表, 默认去重
     * @param array $columns 原料表的字段，默认显示全部
     * @return array 默认keyBy('id')
     */
    public function getByIdList(array $idList, array $columns = ['*']): array;

    /**
     * 通过内部code列表获取原料列表
     * @param array $codeList 默认去重
     * @param array $columns
     * @return array 默认keyBy('internal_code')
     */
    public function getByCodeList(array $codeList, array $columns = ['id']);

    /**
     * 获取指定品名下所有的原料数据
     * @param array $nameIds 默认去重
     * @param array $columns
     * @return array 默认groupBy('material_name_id')
     */
    public function getListByNameIds(array $nameIds, array $columns = ['*']);

    /**
     * 通过原料品名id列表获取原料品名数组
     * @param array $idList 原料品名id的列表, 默认去重
     * @param bool $withMaterials 是否关联原料数据
     * @param array $columns 原料品名表的字段，默认显示全部
     * @return array 默认keyBy('id')
     */
    public function getMaterialNamesByIdList(array $idList, $withMaterials = false, array $columns = ['*']): array;

    /**
     * 获取某原料品类下的所有原料品名
     * @param $categoryId
     * @return array
     */
    public function getMaterialNamesByCategoryId($categoryId);

    /**
     * 判断产品是否绑定原料品名
     * @param $productId
     * @return bool
     */
    public function hasProductMaterialNames($productId): bool;

    /**
     * 获取产品绑定的原料品名
     * @param $productId
     * @return array keyBy('id')
     */
    public function getProductMaterialNames($productId);

    /**
     * 获取产品对应颜色绑定的原料
     * @param $productId
     * @param array $colorIds 默认去重
     * @param bool $withMaterials
     * @return array 默认keyBy('color_id')
     */
    public function getProductColorMaterials($productId, array $colorIds, $withMaterials = false);

    /**
     * 获取维护完全原料数量的颜色id
     * @param $productId
     * @return array
     */
    public function getFullMaterialCountColorIds($productId);

}
