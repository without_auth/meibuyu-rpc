<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2021/12/07
 * Time: 9:39
 */

namespace Meibuyu\Rpc\Service\Interfaces\Product;

interface ThirdPlatformProductServiceInterface
{

    /**
     * 通过id数组获取shopify子产品数组
     * @param array $ids 默认去重
     * @return array 默认keyBy
     * @author Zero
     */
    public function getShopifyChildrenByIds(array $ids): array;

    /**
     * 通过id数组获取速卖通子产品数组
     * @param array $ids 默认去重
     * @return array 默认keyBy
     * @author Zero
     */
    public function getAliExpressChildrenByIds(array $ids): array;

    /**
     * 通过id数组获取Magento子产品数组
     * @param array $ids 默认去重
     * @return array 默认keyBy
     * @author Zero
     */
    public function getMagentoChildrenByIds(array $ids): array;

}
