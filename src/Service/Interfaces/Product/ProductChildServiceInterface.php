<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Rpc\Service\Interfaces\Product;

interface ProductChildServiceInterface
{

    /**
     * 获取单个数据
     * @param int $id 子SKU id
     * @param array $columns 子SKU表的字段，默认显示全部
     * @param array $relations 子SKU的关联关系,可传入['brand', 'category', 'product_name', 'images', 'cost', 'weight', 'packs']
     * @return array|null
     */
    public function get($id, array $columns = ['*'], array $relations = []);

    /**
     * 通过id列表获取产品数组
     * @param array $idList 子SKUid的列表, 默认去重
     * @param array $columns 子SKU表的字段，默认显示全部
     * @param array $relations 子SKU的关联关系,可传入['brand', 'category', 'product_name', 'images', 'cost', 'weight', 'packs']
     * @return array 默认keyBy('id')
     */
    public function getByIdList(array $idList, array $columns = ['*'], array $relations = []): array;

    /**
     * 通过sku列表获取子产品列表
     * @param array $skuList 默认去重
     * @param array $columns 子SKU表的字段，默认返回id
     * @param array $relations 子SKU的关联关系,可传入['color', 'size','brand', 'category', 'product_name', 'images', 'cost', 'weight', 'packs']
     * @return array 默认keyBy('child_sku')
     */
    public function getListBySkuList(array $skuList, array $columns = ['id'], array $relations = []);

    /**
     * 获取全部尺码列表
     * @param array $columns 默认['id', 'name']
     * @return array
     */
    public function sizes(array $columns = ['id', 'name']): array;

    /**
     * 获取全部颜色列表
     * @param array $columns 默认['id', 'code', 'cn_name', 'en_name']
     * @return array
     */
    public function colors(array $columns = ['id', 'code', 'cn_name', 'en_name']): array;

    /**
     * 通过id数组获取尺码列表
     * @param array $ids 默认去重
     * @param array $columns
     * @return array 默认keyBY('id')
     */
    public function getSizesByIds(array $ids, $columns = ['id', 'name']): array;

    /**
     * 通过id数组获取颜色列表
     * @param array $ids 默认去重
     * @param array $columns
     * @return array 默认keyBY('id')
     */
    public function getColorsByIds(array $ids, array $columns = ['id', 'code', 'cn_name', 'en_name']): array;

    /**
     * 通过id获取尺码
     * @param int $id
     * @param array $columns
     * @return array|null
     */
    public function getSizeById(int $id, $columns = ['id', 'name']);

    /**
     * 通过id获取颜色
     * @param int $id
     * @param array $columns
     * @return array|null
     */
    public function getColorById(int $id, array $columns = ['id', 'code', 'cn_name', 'en_name']);

    /**
     * 获取某产品下的所有子产品
     * @param int $productId
     * @param array $columns
     * @return array
     */
    public function getListByProductId(int $productId, $columns = ['id', 'child_sku']);

    /**
     * 模糊搜索仓库子sku,获取id数组
     * @param string $childSku 仓库子sku
     * @param array|null $limitIds 限制id数组,不传为不限制
     * @return array
     */
    public function getIdsByChildSku(string $childSku, array $limitIds = null);

    /**
     * 根据主sku品类id获取仓库子sku的id数组
     * @param int $categoryId
     * @param int|null $limit
     * @return array
     * @author Zero
     */
    public function getIdsByCategoryId(int $categoryId, int $limit = null);

}
