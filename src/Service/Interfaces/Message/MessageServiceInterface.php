<?php
/**
 * Created by PhpStorm.
 * User: 姜克保
 * Date: 2020/5/20
 * Time: 15:48
 */

namespace Meibuyu\Rpc\Service\Interfaces\Message;

interface MessageServiceInterface
{
    /**
     * @param array $receiveUserId 接收人id
     * @param string $application 应用名称
     * @param int $sendUserId 发送人id(发送人id等于0时是系统消息)
     * @param int $templateId 模板id(等于0时直接使用content内容)
     * @param array $replace 替换模板内容
     * @param string $content 自定义消息内容
     * @return bool
     */
    public function send(array $receiveUserId, $application, $templateId = 0, $sendUserId = 0, $replace = [], $content = ''): bool;

    /**
     * description:发送邮件
     * author: fuyunnan
     * @param array $userList ['111@qq.com','222@qq.com']
     * @param string $subject 邮件标题
     * @param string $body 邮件内容
     * @return bool
     * @throws
     * Date: 2020/9/10
     */
    public function sendMail($userList, $subject, $body): bool;

    /**
     * 发送markdown文本消息
     * @param array $receiveUserId
     * @param $application
     * @param int $templateId
     * @param int $sendUserId
     * @param array $replace
     * @param string $content
     * @param string $title
     * @return bool
     */
    public function SendMarkDownMessage(array $receiveUserId, $application, $templateId = 0, $sendUserId = 0, $replace = [], $content = '', $title = ''): bool;

    /**
     * 发送短信
     * @param string $phone 电话号码  示例'175523181**'
     * @param string $templateCode 模板code 1.取消订单通知 'SMS_205395200' 2.派单提醒 'SMS_205390191' 3.返工提醒 'SMS_205390196' 4.通知类型 'SMS_205134732'
     * @param array $params 模板中替换的变量  示例传入格式：['product'=>'AE1222']
     * @return bool
     */
    public function sendSms(string $phone, string $templateCode, array $params): bool;

}
