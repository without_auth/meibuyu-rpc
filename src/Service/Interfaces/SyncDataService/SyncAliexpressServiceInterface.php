<?php
/**
 * Created by PhpStorm.
 * User: qiudongfang
 * Date: ${DATA}
 * Time: 下午2:57
 */

namespace Meibuyu\Rpc\Service\Interfaces\SyncDataService;

interface SyncAliexpressServiceInterface
{
    /**
     * @param integer $siteId
     * @param integer $status
     * @param array $orderIds
     * @return mixed
     */
    public function syncOrders($siteId, $status, $orderIds);

    /**
     * @param integer $siteId
     * @param array $orderIds
     * @param integer $status
     * @return mixed
     */
    public function updateOrderStatus($siteId, $orderIds, $status);

    /**
     * @param integer $siteId
     * @param array $productIds
     * @return mixed
     */
    public function syncProducts($siteId, $productIds);

    /**
     * @param integer $siteId
     * @param array $productIds
     * @param integer $status
     * @return mixed
     */
    public function updateProductStatus($siteId, $productIds, $status);
}
