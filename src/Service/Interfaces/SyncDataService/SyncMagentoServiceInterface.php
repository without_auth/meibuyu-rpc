<?php
/**
 * Created by PhpStorm.
 * User: qiudongfang
 * Date: ${DATA}
 * Time: 下午2:57
 */

namespace Meibuyu\Rpc\Service\Interfaces\SyncDataService;

interface SyncMagentoServiceInterface
{
    /**
     * @param int $platformOrderId 平台单号id
     * @param int $pageSize 每页最多展示50条
     * @return bool
     */
    public function syncOrders(int $platformOrderId, $pageSize = 10):bool;
}
