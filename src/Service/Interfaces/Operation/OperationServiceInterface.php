<?php
/**
 * Created by PhpStorm.
 * User: zhaopeng
 * Date: 2020/9/5
 * Time: 10:09
 */

namespace Meibuyu\Rpc\Service\Interfaces\Operation;

interface OperationServiceInterface
{
    /**
     * 获取销售报表其他数据
     * @param array $conditions 必传条件  site_id team_id start_time end_time
     * @return mixed
     */
    public function getBusinessSaleOtherDatum(array $conditions);

    /**
     * 获取销售报表其他数据
     * @param array $conditions 必传条件  site_id team_id start_time end_time
     * @return mixed
     */
    public function getBusinessSaleOtherDatumRmb(array $conditions);
}
